<?php namespace Tests\Feature;

use App\Jobs\UserOrderJob;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;


class KauflandTest extends TestCase
{
    /** @test */
    public function user_order_command_sends_an_order_queue(): void
    {
        Queue::fake();
        $this->artisan('order --amount=500');
        
        Queue::assertPushedOn('order', UserOrderJob::class);
    }
    
    /** @test
     * @throws \Exception
     */
    public function check_artisan_command_amount_parameter(): void
    {
        Queue::fake();
        $amount = random_int(100, 1000);
        $this->artisan("order --amount=$amount");
        
        Queue::assertPushed(function (UserOrderJob $job) use ($amount) {
            return $job->orderModel->amount === $amount;
        });
    }
    
    /** @test */
    public function an_order_always_has_a_user_and_email(): void
    {
        Queue::fake();
        $this->artisan("order --amount=500");
        Queue::assertPushed(function (UserOrderJob $job) {
            return $job->orderModel->name !== null;
        });
        
        $this->artisan("order --amount=500");
        Queue::assertPushed(function (UserOrderJob $job) {
            return $job->orderModel->email !== null;
        });
    }
}