<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class GetOrderResultBackCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orderResult';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get order results back using redis , pub/sub';
    
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Redis::subscribe(['order-result'], function ($message) {
            echo PHP_EOL.$message.PHP_EOL;
        });
        
        return 0;
    }
}
