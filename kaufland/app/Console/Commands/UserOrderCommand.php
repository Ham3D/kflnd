<?php

namespace App\Console\Commands;

use App\Exceptions\RunSeederFirstException;
use App\Jobs\UserOrderJob;
use App\Models\OrderModel;
use App\Models\User;
use Exception;
use Illuminate\Console\Command;

class UserOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order {--amount=}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'an order happened';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return int
     * @throws RunSeederFirstException
     * @throws Exception
     */
    public function handle(): int
    {
        // pick a random user
        $orderModel = new OrderModel();
        
        $user = User::inRandomOrder()->first();
        
        if ($user === null) {
            throw new RunSeederFirstException();
        }
        
        
        $orderModel->name   = $user->name;
        $orderModel->email  = $user->email;
        $orderModel->amount = (float) $this->option('amount');
        
        dispatch(new UserOrderJob($orderModel));
        
        $this->reportOrder($orderModel);
        
        return 0;
    }
    
    private function reportOrder(OrderModel $orderModel): void
    {
        $this->info($orderModel->name.' has and order costs '.$orderModel->amount.'€');
    }
    
}
