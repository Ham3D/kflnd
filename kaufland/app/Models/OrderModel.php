<?php namespace App\Models;

class OrderModel
{
    public int $amount;
    public bool $status;
    public string $name;
    public string $email;
}