<?php

namespace App\Jobs;

use App\Models\OrderModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public OrderModel $orderModel;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderModel $orderModel)
    {
        $this->onQueue('order');
        
        $this->orderModel = $orderModel;
    }
}
