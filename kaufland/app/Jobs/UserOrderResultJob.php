<?php

namespace App\Jobs;

use App\Models\OrderModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JsonException;

class UserOrderResultJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public OrderModel $orderModel;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderModel $orderModel)
    {
        $this->onQueue('orderResult');
        $this->orderModel = $orderModel;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     * @throws JsonException
     */
    public function handle(): void
    {
        echo "\n".json_encode($this->orderModel, JSON_THROW_ON_ERROR)."\n";
    }
}
