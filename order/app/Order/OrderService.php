<?php namespace App\Order;

use App\Jobs\HandleDiscountJob;
use App\Jobs\UserOrderResultJob;
use App\Models\OrderModel;
use Exception;
use Illuminate\Support\Facades\Redis;
use JsonException;

class OrderService
{
    /**
     * @throws JsonException
     * @throws Exception
     */
    public function handle(OrderModel $order): OrderModel
    {
        $order = $this->processOrder($order);
        
        // handle discount only if orderStatus is true
        if ($order->status) {
            dispatch(new HandleDiscountJob($order));
        }
        
        // handle order notification
        (new OrderNotificationService())->handle($order);
        
        // report back order details to Kaufland
        $this->reportOrderBackToKaufland($order);
        
        return $order;
    }
    
    /**
     * nothing much is done here, only setting a status randomly on the order
     * @throws Exception
     */
    public function processOrder(OrderModel $order): OrderModel
    {
        // order status can be true or false [succeed or failed]
        $order->status = random_int(0, 1) === 1;
        
        return $order;
    }
    
    /**
     * it's handled by two different methods
     * @throws JsonException
     */
    private function reportOrderBackToKaufland(OrderModel $order): void
    {
        // using queues
        dispatch(new UserOrderResultJob($order));
        
        // using redis -> pub/sub
        Redis::publish('order-result', json_encode($order, JSON_THROW_ON_ERROR));
    }
    
}