<?php namespace App\Order;

use App\Jobs\EmailJob;
use App\Models\EmailModel;
use App\Models\OrderModel;

class OrderNotificationService
{
    public OrderModel $order;
    public EmailModel $emailModel;
    
    public function __construct()
    {
        $this->emailModel = new EmailModel();
    }
    
    public function handle(OrderModel $order): void
    {
        $this->order = $order;
        $this->preProcessEmail();
        
        if ($order->status) {
            $this->emailModel->message = "Order successful, Thanks!";
        } else {
            $this->emailModel->message = "Order Failed, Sorry!";
        }
        
        dispatch(new EmailJob($this->emailModel));
    }
    
    private function preProcessEmail(): void
    {
        $this->emailModel->name    = $this->order->name;
        $this->emailModel->address = $this->order->email;
    }
}