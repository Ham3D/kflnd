<?php

namespace App\Jobs;

use App\Models\OrderModel;
use App\Order\OrderService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use JsonException;

class UserOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public OrderModel $orderModel;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderModel $orderModel)
    {
        $this->onQueue('order');
        
        $this->orderModel = $orderModel;
    }
    
    /**
     * Execute the job.
     *
     * @param  OrderService  $orderService
     *
     * @return void
     * @throws JsonException
     */
    public function handle(OrderService $orderService): void
    {
        $orderService->handle($this->orderModel);
    }
}
