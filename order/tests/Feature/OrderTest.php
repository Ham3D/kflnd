<?php namespace Tests\Feature;

use App\Jobs\EmailJob;
use App\Jobs\HandleDiscountJob;
use App\Jobs\UserOrderJob;
use App\Jobs\UserOrderResultJob;
use App\Models\EmailModel;
use App\Models\OrderModel;
use App\Order\OrderService;
use Faker\Factory;
use Illuminate\Support\Facades\Queue;
use JsonException;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /** @test */
    public function it_receives_order_queue(): void
    {
        Queue::fake();
        $orderModel = $this->seedOrderModel();
        dispatch(new UserOrderJob($orderModel));
        
        Queue::assertPushedOn('order', UserOrderJob::class);
    }
    
    /** @test
     * @throws JsonException
     */
    public function if_order_is_a_success_a_discount_job_is_triggered(): void
    {
        Queue::fake();
        $orderModel = $this->seedOrderModel();
        
        $orderModel = (new OrderService())->handle($orderModel);
        
        if ($orderModel->status) {
            Queue::assertPushedOn('discount', HandleDiscountJob::class);
        } else {
            Queue::assertNotPushed('discount', HandleDiscountJob::class);
        }
    }
    
    /** @test
     * @throws JsonException
     */
    public function after_each_order_there_will_be_notifications(): void
    {
        Queue::fake();
        
        $orderService = new OrderService();
        $orderModel   = $this->seedOrderModel();
        $emailModel   = $this->seedEmailModel();
        
        $orderService->handle($orderModel);
        
        dispatch(new EmailJob($emailModel));
        
        Queue::assertPushedOn('email', EmailJob::class);
    }
    
    /** @test
     * @throws JsonException
     */
    public function after_processing_an_order_there_will_be_report_back_to_kaufland(): void
    {
        Queue::fake();
        
        $orderService = new OrderService();
        $orderModel   = $this->seedOrderModel();
        $orderService->handle($orderModel);
        
        Queue::assertPushedOn('orderResult', UserOrderResultJob::class);
    }
    
    private function seedOrderModel(): OrderModel
    {
        $faker      = Factory::create();
        $orderModel = new OrderModel();
        
        $orderModel->name   = $faker->name;
        $orderModel->email  = $faker->email;
        $orderModel->amount = $faker->numberBetween(100, 1000);
        
        return $orderModel;
    }
    
    private function seedEmailModel(): EmailModel
    {
        $faker      = Factory::create();
        $emailModel = new EmailModel();
        
        $emailModel->name    = $faker->name;
        $emailModel->address = $faker->email;
        $emailModel->message = $faker->text(2000);
        
        return $emailModel;
    }
}