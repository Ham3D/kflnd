
## Kaufland disturbuted systems Trial task  
  
### Summery  
we have 4 microsystems here as listed below, I used the Redis queue system to handle the job, also small usage of the Redis pub-sub feature to deliver messages between different systems in nearly real-time!  
  
our systems are :  
- Kaufland  
- order  
- discount  
- email  
  
the Bussines rule is :  
> If an order that costs more than 100 euro gets fulfilled, the customer will receive a voucher worth 5 euro.  
  
the designed and coded system is fail-proof, which means if you shut down any of the services the task is cached(in Broker, Redis here), and will be done after the service come back online.  
  
> the only flaw I can see is the failure of the Broker (**Redis**) but there are also solutions for that.  
> we need to implement a cluster of different servers. the problem here is Redis doesn't support multi-master architecture by default, Redis clusters will have one master and many clients that we don't need. that one master is **a point of failure**.  
> what we can do is using a master-master solution like what Netflix did with Dynomite `https://github.com/Netflix/dynomite` .  
  
so in short, when an order is registered at Kaufland (our main service) there is a guarantee that we handle that order (now, or if the required service is offline, sometimes later when that service comes back online) and other services like discounts and email, etc.  
  
## System requirements  
- php > 7.4 , i used php 8  
- redis > 5.0 , i used ver 6.2.1  
- mysql(or maria db) , i used mysql 8 (for seeding some fake users)  
  
## How to Install  
- clone the repo on local  
- ```composer install``` in all services  
- ``` cp .env.example .env``` and configure .env , **bad practice to store env in git, but for this demo its fine**  
  
## How to use the system  
open 5 terminal windows, 4 terminal windows for our 4 services, and one more to check email logs  
  
### Kaufland Service 
we have a users table in this service, and a seeder to seed some fake users.
after configuring database info in .env do : 
``` php artisan migrate --seed```
 
in the Kaufland service, we may use the below command to create an order  
```php artisan order --amount 500```  
  
with this command, Kaufland service will publish an order worth 500€  
  
  
also, we can listen to the result of the order (the order service sends back the result of the order)  
**We have two options here**  
- use the queue system as of other services used it  
```php artisan queue:listen --queue orderResult```  
  
- or using Redis pub/sub features which a lot faster O(1), but has a con when there are no listeners(subscribers) published messages are discarded  
  
``` php artisan orderResult```  
  
> I liked to add the Redis pub-sub method to demonstrate a way to send messages between different services, applications, and even languages, in real-time.  
  
above methods, both work as expected (the second one is a lot faster but with its limitations).  
  
### Order Service  
in **Orders** folder run this command :  
This service handles order-related things.  
> there is only one thing done here as order logic. order service changes order status randomly so an order might fail or succeed.  
  
```php artisan queue:work --queue order```  
  
  
### Discount Service  
This service checks if the user should receive a voucher for his order.  
>how I coded this is violating Single responsibility(i send order model to discount service, we need to refactor it and remove order related stuff from it)  
  
in **Discount** folder run this commnad :  
```php artisan queue:work --queue discount```  
  
### Email Service  
This service sends emails.[or it should, we just log the message]  
  
![well, self explanatory](https://res.cloudinary.com/dh75yeyur/image/upload/v1632652107/pizza_dzbwlb.jpg)  
  
in **Email** folder run this command :  
```php artisan queue:work --queue email```  
  
**because at the start we don't have that ```laravel.log``` file, the below command will give you an error, run it after sending at last one Order**  
  
``` tail -f ./storage/logs/laravel.log```  
  
---  
**at this point, our services are up and listening and waiting for an order to publish.**  
  
### how to see the actual work  
well, run the services, and send an order from Kaufland.  
you can see the processing of jobs in each service and also check the email logs.  
  
## Test  
there are some tests for each service, you can test the system with one of the below commands.  
  
- ```php artisan test```  
- ```phpunit```  
  
  ## System Design
  ![enter image description here](https://res.cloudinary.com/dh75yeyur/image/upload/v1632656434/Kaufland_bzqbbb.png)
---  
in the end, I like to add some notes to my solution.  

First, I like to share my thoughts about this solution, also some other things that I didn't put here, which mean maybe another small meeting?
  
Second, as far as I understand, the actual purpose of this trial task is to test my system design as architecture and not much coding.  
so I didn't put much time on actual coding, adding details, write more tests, etc.  
ofc I can do that if that's the requirement.
  
Best regards.  
Hamed