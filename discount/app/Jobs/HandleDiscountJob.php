<?php

namespace App\Jobs;

use App\Discount\DiscountService;
use App\Models\OrderModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleDiscountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public OrderModel $order;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderModel $order)
    {
        $this->onQueue('discount');
        
        $this->order = $order;
    }
    
    /**
     * Execute the job.
     *
     * @param  DiscountService  $service
     *
     * @return void
     */
    public function handle(DiscountService $service): void
    {
        $service->handle($this->order);
    }
}
