<?php namespace App\Discount;

use App\Models\OrderModel;

class DiscountService
{
    private VoucherGenerationService $voucherGenerationService;
    private NotificationService $notificationService;
    
    public function __construct()
    {
        $this->voucherGenerationService = new VoucherGenerationService();
        $this->notificationService      = new NotificationService();
    }
    
    public function handle(OrderModel $order): void
    {
        $this->checkPolicyOne($order);
    }
    
    /**
     * if user buy things worth more than 100€, user receive 5€ voucher
     * Worst naming, I know!
     */
    private function checkPolicyOne(OrderModel $order): void
    {
        if ($order->amount > 100) {
            $voucher = $this->voucherGenerationService->handle(5);
            
            $this->notificationService->emailVoucher($order->name, $order->email, $voucher);
        }
    }
    
    
}