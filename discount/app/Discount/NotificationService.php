<?php namespace App\Discount;

use App\Jobs\EmailJob;
use App\Models\EmailModel;
use App\Models\VoucherModel;

class NotificationService
{
    public function emailVoucher(string $name, string $emailAddress, VoucherModel $voucher): void
    {
        $email = new EmailModel();
        
        $email->name    = $name;
        $email->address = $emailAddress;
        $email->message = "Enjoy your new voucher worth {$voucher->amount}€ \n
        Voucher code: $voucher->code";
        
        dispatch(new EmailJob($email));
    }
    
}