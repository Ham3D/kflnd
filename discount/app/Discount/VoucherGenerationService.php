<?php namespace App\Discount;

use App\Models\VoucherModel;
use Illuminate\Support\Str;

class VoucherGenerationService
{
    /**
     * generate voucher class main entry
     * we have to generate voucher, save it and return it
     *
     * @param  int  $amount
     *
     * @return VoucherModel
     */
    public function handle(int $amount): VoucherModel
    {
        return $this->generateVoucher($amount);
    }
    
    /**
     * @param  int  $amount
     *
     * @return VoucherModel
     */
    private function generateVoucher(int $amount): VoucherModel
    {
        $newVoucher         = new VoucherModel();
        $newVoucher->amount = $amount;
        $newVoucher->code   = Str::random(10);
        
        return $newVoucher;
    }
    
}