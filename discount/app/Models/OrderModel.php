<?php namespace App\Models;

class OrderModel
{
    public float $amount;
    public bool $status;
    public string $name;
    public string $email;
}