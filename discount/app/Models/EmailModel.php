<?php namespace App\Models;

class EmailModel
{
    public string $name;
    public string $address;
    public string $message;
}