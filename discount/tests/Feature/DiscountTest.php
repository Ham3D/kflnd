<?php namespace Tests\Feature;

use App\Discount\DiscountService;
use App\Jobs\EmailJob;
use App\Models\OrderModel;
use Faker\Factory;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class DiscountTest extends TestCase
{
    /** @test */
    public function if_user_buy_more_than_100_he_should_receive_5_voucher(): void
    {
        Queue::fake();
        $order           = $this->seedOrderModel(true);
        $discountService = new DiscountService();
        
        //$order->amount = 99; // how to test if the test actually works! x_0
        $discountService->handle($order);
        if ($order->amount > 100) {
            Queue::assertPushed(function (EmailJob $job) {
                return strpos($job->email->message, 'Enjoy your new voucher') !== false;
            });
        } else {
            Queue::assertNotPushed(EmailJob::class);
        }
    }
    
    private function seedOrderModel(bool $status = null): OrderModel
    {
        $faker      = Factory::create();
        $orderModel = new OrderModel();
        
        $orderModel->name   = $faker->name;
        $orderModel->email  = $faker->email;
        $orderModel->amount = $faker->numberBetween(10, 1000);
        
        if (isset($status)) {
            $orderModel->status = $status;
        } else {
            $orderModel->status = $faker->boolean;
        }
        
        return $orderModel;
    }
}