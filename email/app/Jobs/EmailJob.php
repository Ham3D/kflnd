<?php

namespace App\Jobs;

use App\Email\EmailService;
use App\Models\EmailModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public EmailModel $email;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(EmailModel $email)
    {
        $this->onQueue('email');
        
        $this->email = $email;
    }
    
    /**
     * Execute the job.
     *
     * @param  EmailService  $emailService
     *
     * @return void
     */
    public function handle(EmailService $emailService): void
    {
        $emailService->handle($this->email);
    }
}
