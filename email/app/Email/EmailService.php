<?php namespace App\Email;

use App\Models\EmailModel;
use Illuminate\Support\Facades\Log;

class EmailService
{
    /**
     * sending emails
     *
     * @param  EmailModel  $emailModel
     */
    public function handle(EmailModel $emailModel): void
    {
        $info = "\n Sending Email to $emailModel->address ($emailModel->name) \n 
        Message : $emailModel->message \n";
        Log::info($info);
    }
    
}