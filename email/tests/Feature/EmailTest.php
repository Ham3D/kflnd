<?php namespace Tests\Feature;

use App\Email\EmailService;
use App\Models\EmailModel;
use Faker\Factory;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class EmailTest extends TestCase
{
    /** @test */
    public function it_can_sends_email(): void
    {
        // we actually save some logs
        
        Log::shouldReceive('info')->once()
           ->withArgs(function ($message) {
               return strpos($message, 'Sending Email to') !== false;
           });
        
        $emailService = new EmailService();
        $emailService->handle($this->emailModelSeeder());
        
        $this->assertTrue(true);
    }
    
    private function emailModelSeeder(): EmailModel
    {
        $faker      = Factory::create();
        $emailModel = new EmailModel();
        
        $emailModel->address = $faker->email;
        $emailModel->message = $faker->text(100);
        $emailModel->name    = $faker->name;
        
        return $emailModel;
    }
}